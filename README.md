OgoXe techtool


************************************************

Application facilitant la logistique d’intervention technique destiné aux techniciens sur site.
Fournit une vue globale des sites de station Ogoxe sur la région sous forme de carte balisée et signale leurs états de fonctionnement:« Opérationnel », « Alerte disfonctionnement » et « en manutention ».

************************************************

# OgoXe TechTool – Guide d’installation

The project was made using Symfony 5. You should refer to the official documentation for more information. (https://symfony.com/doc/current/index.html)

**1. Make sure the following programs are installed :**

   * composer : https://getcomposer.org/
   * PHP (>= 7.4.4)

**2. Clone Repository from official Github of Ogoxi :** 

"https://github.com/ogoxi/ogoxe_techtool.git"

**3. Go to project folder, run the following command :**
```
$ composer install
```

**4. Modify ".env" : Change following line to correspond to your database settings.**
``` FILE
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```
For example
``` FILE
DATABASE_URL=mysql://root:qwerty@127.0.0.1:3306/ogoxe_techtool
```

**5. Create database using command :**
```
If the database doesn't exist yet, run this command :

$ php bin/console doctrine:database:create
$ php bin/console make:migration
$ php bin/console doctrine:migrations:migrate
```

**for dev:

php bin/console doctrine:fixtures:load


**6. Run command to start a local server.**
```
$ php symfony serve
```
Visit "http://127.0.0.1:8000/" to start the application.