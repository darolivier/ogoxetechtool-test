<?php


namespace App\Controller;

use App\Repository\BalisesRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClientController extends AbstractController
{

    /**
     * @Route("/myometrix", name="ClientPage")
     * 
     */
    public function index(BalisesRepository $repo)
    {
        $user = $this->getUser();
        $username = $user->getUsername();

        $balises = $repo->findAll();



        return $this->render('client.html.twig', [
            'controller_name' => 'ClientController',
            'username' => $username,
            'balises' => $balises,

        ]);
    }
}
