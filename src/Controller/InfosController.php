<?php

namespace App\Controller;

use App\Repository\BalisesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InfosController extends AbstractController
{
    /**
     * @Route("/infos", name="infos")
     * 
     */
    public function info(BalisesRepository $repo)
    {

        $balises = $repo->findAll();
        return $this->render('index.html.twig', [
            'controller_name' => 'InfosController',
            'balises' => $balises
        ]);
    }
}
