<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200409091811 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE balises (id INT AUTO_INCREMENT NOT NULL, etat_id INT NOT NULL, nom VARCHAR(255) NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, INDEX IDX_DF3015E5D5E86FF (etat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE capteur (id INT AUTO_INCREMENT NOT NULL, balise_id_id INT NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_5B4A1695AA128858 (balise_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE etat (id INT AUTO_INCREMENT NOT NULL, etat VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention (id INT AUTO_INCREMENT NOT NULL, balise_id_id INT DEFAULT NULL, date DATE NOT NULL, type VARCHAR(255) NOT NULL, commentaires VARCHAR(255) DEFAULT NULL, INDEX IDX_D11814ABAA128858 (balise_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE balises ADD CONSTRAINT FK_DF3015E5D5E86FF FOREIGN KEY (etat_id) REFERENCES etat (id)');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A1695AA128858 FOREIGN KEY (balise_id_id) REFERENCES balises (id)');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814ABAA128858 FOREIGN KEY (balise_id_id) REFERENCES balises (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A1695AA128858');
        $this->addSql('ALTER TABLE intervention DROP FOREIGN KEY FK_D11814ABAA128858');
        $this->addSql('ALTER TABLE balises DROP FOREIGN KEY FK_DF3015E5D5E86FF');
        $this->addSql('DROP TABLE balises');
        $this->addSql('DROP TABLE capteur');
        $this->addSql('DROP TABLE etat');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('DROP TABLE user');
    }
}
