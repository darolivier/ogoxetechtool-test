<?php

namespace App\DataFixtures;

use App\Entity\Etat;
use App\Entity\Balises;
use App\Entity\Capteur;
use App\Entity\Intervention;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $etat1 = new Etat();
        $etat1->setEtat('operationnel');
        $manager->persist($etat1);

        $etat2 = new Etat();
        $etat2->setEtat('alerte');
        $manager->persist($etat2);

        $etat3 = new Etat();
        $etat3->setEtat('manutention');
        $manager->persist($etat3);


        $balise1 = new Balises();
        $balise1->setNom('toulouse');
        $balise1->setLatitude('43.6044622');
        $balise1->setLongitude('1.4442469');
        $balise1->setEtat($etat1);
        $manager->persist($balise1);

        $balise2 = new Balises();
        $balise2->setNom('toulouse2');
        $balise2->setLatitude('43.6544622');
        $balise2->setLongitude('1.6442469');
        $balise2->setEtat($etat2);
        $manager->persist($balise2);

        $balise3 = new Balises();
        $balise3->setNom('Mauléon Barousse');
        $balise3->setLatitude('42.959');
        $balise3->setLongitude('0.567');
        $balise3->setEtat($etat1);
        $manager->persist($balise3);

        $balise4 = new Balises();
        $balise4->setNom('Izaourt (église)');
        $balise4->setLatitude('43.016');
        $balise4->setLongitude('0.6');
        $balise4->setEtat($etat1);
        $manager->persist($balise4);

        $balise5 = new Balises();
        $balise5->setNom('Ferrere - Chalets St Neree (Hauteur)');
        $balise5->setLatitude('42.934');
        $balise5->setLongitude('0.511');
        $balise5->setEtat($etat3);
        $manager->persist($balise5);

        $capteur1 = new Capteur();
        $capteur1->setNom('pluviomètre');
        $capteur1->setBaliseId($balise1);
        $manager->persist($capteur1);

        $capteur2 = new Capteur();
        $capteur2->setNom('hydrometre');
        $capteur2->setBaliseId($balise2);
        $manager->persist($capteur2);

        $capteur3 = new Capteur();
        $capteur3->setNom('hydrometre modele 1');
        $capteur3->setBaliseId($balise3);
        $manager->persist($capteur3);

        $capteur4 = new Capteur();
        $capteur4->setNom('cadran');
        $capteur4->setBaliseId($balise4);
        $manager->persist($capteur4);

        $capteur5 = new Capteur();
        $capteur5->setNom('anénomètre');
        $capteur5->setBaliseId($balise5);
        $manager->persist($capteur5);





        $intervention1 = new Intervention();
        $intervention1->setDate(new \dateTime());
        $intervention1->setType('réparation cables');
        $intervention1->setCommentaires('pas facile, il pleuvait, il faudra revenir pour verifier');
        $intervention1->setBaliseId($balise1);
        $manager->persist($intervention1);

        $intervention2 = new Intervention();
        $intervention2->setDate(new \dateTime());
        $intervention2->setType('réparation cadran');
        $intervention2->setCommentaires('rien à signaler');
        $intervention2->setBaliseId($balise2);
        $manager->persist($intervention2);

        $intervention3 = new Intervention();
        $intervention3->setDate(new \dateTime());
        $intervention3->setType('réparation cables');
        $intervention3->setCommentaires('je me suis fait tirer dessus par des chasseurs');
        $intervention3->setBaliseId($balise3);
        $manager->persist($intervention3);

        $intervention4 = new Intervention();
        $intervention4->setDate(new \dateTime());
        $intervention4->setType('réparation cables');
        $intervention4->setCommentaires('manque une pièce pour afficher les infos');
        $intervention4->setBaliseId($balise4);
        $manager->persist($intervention4);






        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
