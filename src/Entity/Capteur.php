<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CapteurRepository")
 */
class Capteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Balises", inversedBy="capteurs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $balise_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getBaliseId(): ?Balises
    {
        return $this->balise_id;
    }

    public function setBaliseId(?Balises $balise_id): self
    {
        $this->balise_id = $balise_id;

        return $this;
    }
}
