<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $etat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Balises", mappedBy="etat")
     */
    private $balise_id;

    public function __construct()
    {
        $this->balise_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|Balises[]
     */
    public function getBaliseId(): Collection
    {
        return $this->balise_id;
    }

    public function addBaliseId(Balises $baliseId): self
    {
        if (!$this->balise_id->contains($baliseId)) {
            $this->balise_id[] = $baliseId;
            $baliseId->setEtat($this);
        }

        return $this;
    }

    public function removeBaliseId(Balises $baliseId): self
    {
        if ($this->balise_id->contains($baliseId)) {
            $this->balise_id->removeElement($baliseId);
            // set the owning side to null (unless already changed)
            if ($baliseId->getEtat() === $this) {
                $baliseId->setEtat(null);
            }
        }

        return $this;
    }
}
