<?php

namespace App\Repository;

use App\Entity\Balises;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Balises|null find($id, $lockMode = null, $lockVersion = null)
 * @method Balises|null findOneBy(array $criteria, array $orderBy = null)
 * @method Balises[]    findAll()
 * @method Balises[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BalisesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Balises::class);
    }




    // /**
    //  * @return Balises[] Returns an array of Balises objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Balises
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
