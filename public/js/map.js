console.log("js loaded")

let infos = null;



//initalisation de la carte

function init() {

    infos = document.getElementById('infos');


    //station de configuration de centrage

    const Station = {
        lat: 42.896533,
        lng: -0.113057,
        title: "Gave de Cauterets"
    }

    //génération carte avec attributs de centrage
    const zoomLevel = 8;
    const map = L.map('mapid').setView([Station.lat, Station.lng], zoomLevel);

    //recupération des données de balises du template
    const stations = document.querySelectorAll('[data-id]');
    //recupération des données infos


    //création d'un tableau des stations
    const stationsData = Array.from(stations).map(
        item => item.dataset);
    console.log(stationsData)


    //parcours des stations du tableau et parse des données
    stationsData.forEach(element => {
        let id = parseInt(element.id);
        let nom = element.nom;
        let latitude = parseFloat(element.latitude);
        let longitude = parseFloat(element.longitude);
        let etat = element.etat





        //etablissement de variables pour création des marqueurs
        this.element = {
            id: id,
            lat: latitude,
            lng: longitude,
            title: nom
        };

        //création de la balise relative à la station
        addCustomMarker(this.element, map, etat)
    })

    // requete api openstreetmap par leaflet et Configurations de la carte 
    const mainLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZGFyb2xpdmllciIsImEiOiJjazhvaTc2djEwMnN0M2ZwcjAwZml3aTUwIn0.bftb_0nGVNnxKaHy_tDJrA'
    });

    mainLayer.addTo(map);

}

//intégrer une balise selon longitude et latitude
function addMarker(options, map) {
    const marker = L.marker([options.lat, options.lng], { title: options.title });
    marker.addTo(map);
}

//intégration d'une balise personnalisée selon son état d'alerte
function addCustomMarker(options, map, type) {

    //chemin de l'icone correspond à son état
    const newtype = L.icon({
        iconUrl: '../icons/' + type + '.png'
    })

    const marker = L.marker([options.lat, options.lng], { id: options.id, title: options.title, icon: newtype });
    marker.addTo(map);

    marker.on('click', function (event) {
        console.log("hello click!", event)
        event.target.id = options.id
        console.log(event.target.id)

        infos.innerHTML = `_ Localisation: ${options.title}<br>
                        _ id de la station: ${options.id}`
    })


    //popup sur balise indiquant le nom de la balise
    marker.bindPopup(`<h6><strong>${options.title}</strong></h6>`);

}








